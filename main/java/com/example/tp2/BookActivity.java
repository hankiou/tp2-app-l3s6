package com.example.tp2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {

    TextView nameBook;
    TextView editAuthors;
    TextView editYear;
    TextView editGenres;
    TextView editPublisher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent();
        final long bookId = intent.getLongExtra(MainActivity.selectedBook, -1);
        Book book = null;

        nameBook = findViewById(R.id.nameBook);
        editAuthors = findViewById(R.id.editAuthors);
        editYear = findViewById(R.id.editYear);
        editGenres = findViewById(R.id.editGenres);
        editPublisher = findViewById(R.id.editPublisher);

        if(bookId != -1){
            book = MainActivity.dbh.getBookFromId(bookId);

            nameBook.setText(book.getTitle());
            editAuthors.setText(book.getAuthors());
            editYear.setText(book.getYear());
            editGenres.setText(book.getGenres());
            editPublisher.setText(book.getPublisher());
        }

        Button saveBtn = findViewById(R.id.button);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Book book = new Book(
                    bookId,
                    nameBook.getText().toString(),
                    editAuthors.getText().toString(),
                    editYear.getText().toString(),
                    editGenres.getText().toString(),
                    editPublisher.getText().toString()
                );

                if(bookId == -1) {
                    MainActivity.dbh.addBook(book);
                }else {
                    MainActivity.dbh.updateBook(book);
                }
                finish();
            }
        });
    }
}
